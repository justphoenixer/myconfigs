    #Yeah that's my qtile config i suck at coding

from typing import List  # noqa: F401
import os
import subprocess
from libqtile import bar, layout, widget, hook, extension
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = "alacritty"
browser = "qutebrowser"
fm = "pcmanfm"

keys = [
    # Switch between windows
    # Complete degeneracy with cyrillic kb layout, this fix is bad but works

    Key([mod], "h", lazy.layout.left(),
        desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(),
        desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(),
        desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(),
        desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    Key([mod], "Cyrillic_er", lazy.layout.left(),
        desc="Move focus to left"),
    Key([mod], "Cyrillic_de", lazy.layout.right(),
        desc="Move focus to right"),
    Key([mod], "Cyrillic_o", lazy.layout.down(),
        desc="Move focus down"),
    Key([mod], "Cyrillic_el", lazy.layout.up(),
        desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.

    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(),
        desc="Move window up"),

    Key([mod, "shift"], "Cyrillic_er", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "Cyrillic_de", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "Cyrillic_o", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "Cyrillic_el", lazy.layout.shuffle_up(),
        desc="Move window up"),
    
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.

    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(),
        desc="Grow window up"),

    Key([mod, "control"], "Cyrillic_el", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "Cyrillic_de", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "Cyrillic_o", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "Cyrillic_el", lazy.layout.grow_up(),
        desc="Grow window up"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes

    Key([mod], "Return", lazy.spawn(terminal),
        desc="Launch terminal"),
    Key([mod], "b", lazy.spawn(browser),
        desc="Launch browser"),
    Key([mod], "e", lazy.spawn(fm),
        desc="Launch File Manager"),
    Key([mod], "d", lazy.spawn("discord"),
        desc="Launch discord"),
    Key([mod], "t", lazy.spawn("telegram-desktop"),
        desc="Launch telegram"),

    Key([mod], "Return", lazy.spawn(terminal),
        desc="Launch terminal"),
    Key([mod], "Cyrillic_i", lazy.spawn(browser),
        desc="Launch browser"),
    Key([mod], "Cyrillic_u", lazy.spawn(fm),
        desc="Launch File Manager"),
    Key([mod], "Cyrillic_ve", lazy.spawn("discord"),
        desc="Launch discord"),
    Key([mod], "Cyrillic_ie", lazy.spawn("telegram-desktop"),
        desc="Launch telegram"),

    # Volume keys settings

    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer set Master 1%- unmute")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer set Master 1%+ unmute")),

    # Toggle between different layouts as defined below

    Key([mod, "control"], "space", lazy.widget["keyboardlayout"].next_keyboard(),
        desc="Next keyboard layout."),
    Key([mod], "Tab", lazy.next_layout(),
        desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(),
        desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.restart(),
        desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(),
        desc="Shutdown Qtile"),
    Key([mod], 'p', lazy.run_extension(extension.DmenuRun(
                dmenu_prompt=">",
                dmenu_bottom="true",
                background="#e8e9ec",
                foreground="#33374c",
                selected_background="#8389a3",
                selected_foreground="#2d539e",
                fontsize="13",
    ))),
    Key([mod, "control"], "space", lazy.widget["keyboardlayout"].next_keyboard(),
        desc="Next keyboard layout."),
    Key([mod], "Tab", lazy.next_layout(),
        desc="Toggle between layouts"),
    Key([mod, "shift"], "Cyrillic_es", lazy.window.kill(),
        desc="Kill focused window"),
    Key([mod, "control"], "Cyrillic_ka", lazy.restart(),
        desc="Restart Qtile"),
    Key([mod, "control"], "Cyrillic_shorti", lazy.shutdown(),
        desc="Shutdown Qtile"),
    Key([mod], 'Cyrillic_ze', lazy.run_extension(extension.DmenuRun(
                dmenu_prompt=">",
                dmenu_bottom="true",
                background="#e8e9ec",
                foreground="#33374c",
                selected_background="#8389a3",
                selected_foreground="#2d539e",
                fontsize="13",
    )))
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

layouts = [
    layout.Columns(border_focus_stack=['#b5f0ff'], border_focus=['#b5f0ff'], border_width=1, margin=6),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    layout.Floating(border_focus_stack=['#b5f0ff'], border_focus=['#b5f0ff'], border_width=1),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='Hack',
    fontsize=12,
    padding=2,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar([
		widget.Sep(linewidth=0, padding=6),
        widget.GroupBox(background='dcdfe7', active='2d539e'),
        widget.TextBox(text = '', background='dcdfe7', foreground ='e8e9ec', padding = -1, fontsize = 40),
        widget.WindowName(foreground='33374c'),
        widget.TextBox(text = '', background='e8e9ec', foreground ='dcdfe7', padding = -1, fontsize = 40),
		widget.Sep(linewidth=0, padding=6, background='dcdfe7'),
        widget.Systray(background='dcdfe7'),
		widget.Sep(linewidth=0, padding=6, background='dcdfe7'),
        widget.TextBox(text = '', background='dcdfe7', foreground ='3f83a6', padding = -1, fontsize = 40),
        widget.CurrentLayout(background='3f83a6', foreground='e8e9eC'),
        widget.TextBox(text = '', background='3f83a6', foreground ='dcdfe7', padding = -1, fontsize = 40),
		widget.PulseVolume(background='dcdfe7', foreground='33374c', update_interval=0),
        widget.TextBox(text = '', background='dcdfe7', foreground ='3f83a6', padding = -1, fontsize = 40),
 		widget.KeyboardLayout(configured_keyboards=['us','ua','es'], background='3f83a6', foreground='e8e9eC'),
        widget.TextBox(text = '', background='3f83a6', foreground ='dcdfe7', padding = -1, fontsize = 40),
        widget.Clock(format='%Y-%m-%d %a %I:%M %p', background='dcdfe7', foreground='33374c'),
         ],
            24,
            background='e8e9ec',
#           opacity=0.8,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

@hook.subscribe.startup
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
