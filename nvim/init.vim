" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'itchyny/lightline.vim'
Plug 'cocopon/iceberg.vim'
Plug 'preservim/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'vim-python/python-syntax'
Plug 'vimsence/vimsence'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

let g:lightline = {
      \ 'colorscheme': 'iceberg',
      \ }
colorscheme iceberg
