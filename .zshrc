# My zshrc very bad I'm translator not a fucking coder
### EXPORT
export TERM="xterm-256color"                      # getting proper colors
export HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd -|cd ..)"
export EDITOR="nvim"              # $EDITOR use neovim in terminal
export LANG=uk_UA.UTF-8
### MANPAGER
export MANPAGER="nvim -c 'set ft=man' -"
### VI MODE
bindkey -v
### ALIASES
alias vim="nvim"
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
alias ls="exa -lah --colour=always"
### HISTORY SETTINGS
HISTFILE=~/.zhistory
HISTSIZE=SAVEHIST=10000
### PROMPT SETTING AND STARTUP SHIT
PROMPT="%F{blue}[%f%F{38}%m%f%F{14}@%f%F{123}%n%f %B%F{6}%~%f%b%F{blue}]%f "
neofetch
source ~/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
